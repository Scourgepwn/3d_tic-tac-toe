﻿using UnityEngine;
using System.Collections;

public class RotateBoard : MonoBehaviour
{
    public static bool rotateToggle = true;

    private float baseAngle = 0.0f;

    private void OnMouseDown()
    {
        if (rotateToggle)
        {
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            pos = Input.mousePosition - pos;
            baseAngle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
            baseAngle -= Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;
        }
    }

    private void OnMouseDrag()
    {
        if (rotateToggle)
        {
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            pos = Input.mousePosition - pos;
            float ang = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg - baseAngle;
            transform.rotation = Quaternion.AngleAxis(ang, Vector3.up) * Quaternion.AngleAxis(ang, Vector3.right) * Quaternion.AngleAxis(ang, Vector3.forward);
        }
    }
}
