﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour
{
    private static bool turnOver;

    public static bool p1TurnThreePlus = false;
    public static bool p1TurnError = false;
    public static bool p2TurnError = false;
    public static bool p2TurnThreePlus = false;
    public static bool p1 = true;
    public static bool p2 = false;
    public static int p1i = 0;
    public static int p2i = 0;

    public Text p1Text;
    public Text p2Text;
    public Text warning;
    public Image checkMark;

    public GameObject gameBoard;

    public Color customGreen;
    public Color customBlue;

    private List<GameObject> p1Selected;
    private List<GameObject> p2Selected;

    private void Start()
    {
        warning.text = "";
        p1Selected = new List<GameObject>();
        p2Selected = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        rotationToggleButton();
        endTurn();
        selectCube();
    }

    public void endTurn()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    // First check for win condition
                    // if p1 and win goto p1 victory screen
                    // else if p2 and win goto p2 victory screen

                    if (p1 && turnOver && hit.collider.name == "EndTurnButton")
                    {
                        warning.text = "";
                        p1Text.color = Color.black;
                        p1Text.fontSize = 60;
                        p2Text.color = Color.red;
                        p2Text.fontSize = 120;
                        p1TurnError = false;
                        p1 = false;
                        p2 = true;
                        turnOver = false;
                    }
                    else if (p2 && turnOver && hit.collider.name == "EndTurnButton")
                    {
                        warning.text = "";
                        p2Text.color = Color.black;
                        p2Text.fontSize = 60;
                        p1Text.color = Color.blue;
                        p1Text.fontSize = 120;
                        p2TurnError = false;
                        p2 = false;
                        p1 = true;
                        turnOver = false;
                    }
                    else if (!turnOver && hit.collider.name == "EndTurnButton")
                    {
                        warning.text = "Please make a selection";
                    }
                }
            }
        }
    }

    public void rotationToggleButton()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    if (RotateBoard.rotateToggle && hit.collider.name == "ToggleRotate")
                    {
                        checkMark.enabled = false;
                        gameBoard.layer = 2;
                        RotateBoard.rotateToggle = false;
                    }
                    else if (!RotateBoard.rotateToggle && hit.collider.name == "ToggleRotate")
                    {
                        checkMark.enabled = true;
                        gameBoard.layer = 0;
                        RotateBoard.rotateToggle = true;
                    }
                }
            }
        }
    }

    public void selectCube()
    {
        if (!RotateBoard.rotateToggle)
        {
            //print("HI! i'm all the way out here");
            if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider != null && hit.collider.name != "EndTurnButton" && hit.collider.name != "ToggleRotate")
                    {
                        // P1
                        if (p1 && p1TurnThreePlus && !p1TurnError && hit.collider.tag != "P2" && hit.collider.tag != "P1" && p1i > 2)
                        {
                            print("I Entered Statement " + 1);
                            p1Selected.Add(hit.collider.gameObject);
                            p1Selected[p1i].GetComponent<MeshRenderer>().material.color = Color.blue;
                            p1Selected[p1i].tag = "P1";
                            print(p1Selected.Count);
                            p1Selected[p1i - 3].GetComponent<MeshRenderer>().material.color = customGreen;
                            p1Selected[p1i - 3].tag = "Untagged";
                            p1Selected.Remove(p1Selected[p1i - 3]);
                            print(p1Selected.Count);
                            if (p1i < 3)
                            {
                                p1i++;
                            }
                            print("p1i is " + p1i);
                            p1TurnError = true;
                            turnOver = true;
                            print("I Exited Statement " + 1);
                            return;
                        }
                        else if (p1 && p1TurnError && hit.collider.tag != "P2" && hit.collider.tag != "P1")
                        {
                            print("I Entered Statement " + 2);
                            p1Selected[p1i - 1].GetComponent<MeshRenderer>().material.color = customGreen;
                            p1Selected[p1i - 1].tag = "Untagged";
                            p1Selected.Remove(p1Selected[p1i - 1]);
                            print(p1Selected.Count);
                            p1i--;
                            print("p1i is " + p1i);
                            p1Selected.Add(hit.collider.gameObject);
                            p1Selected[p1i].GetComponent<MeshRenderer>().material.color = Color.blue;
                            p1Selected[p1i].tag = "P1";
                            print(p1Selected.Count);
                            p1i++;
                            print("p1i is " + p1i);
                            print("I Exited Statement " + 2);
                            return;
                        }
                        else if (p1 && !p1TurnThreePlus && !p1TurnError && hit.collider.tag != "P2" && hit.collider.tag != "P1" && p1i < 3)
                        {
                            print("I Entered Statement " + 3);
                            p1Selected.Add(hit.collider.gameObject);
                            p1Selected[p1i].GetComponent<MeshRenderer>().material.color = Color.blue;
                            p1Selected[p1i].tag = "P1";
                            print(p1Selected.Count);
                            if (p1i < 3)
                            {
                                p1i++;
                            }
                            print("p1i is " + p1i);
                            p1TurnError = true;
                            turnOver = true;

                            if (p1i == 3)
                            {
                                p1TurnThreePlus = true;
                            }
                            print("I Exited Statement " + 3);
                            return;
                        }

                        // P2
                        else if (p2 && p2TurnThreePlus && !p2TurnError && hit.collider.tag != "P1" && hit.collider.tag != "P2" && p2i > 2)
                        {
                            print("I Entered Statement " + 4);
                            p2Selected.Add(hit.collider.gameObject);
                            p2Selected[p2i].GetComponent<MeshRenderer>().material.color = Color.red;
                            p2Selected[p2i].tag = "P2";
                            print(p2Selected.Count);
                            p2Selected[p2i - 3].GetComponent<MeshRenderer>().material.color = customGreen;
                            p2Selected[p2i - 3].tag = "Untagged";
                            p2Selected.Remove(p2Selected[p2i - 3]);
                            print(p2Selected.Count);
                            if (p2i < 3)
                            {
                                p2i++;
                            }
                            print("p2i is " + p2i);
                            p2TurnError = true;
                            turnOver = true;
                            print("I Exited Statement " + 4);
                            return;
                        }
                        else if (p2 && p2TurnError && hit.collider.tag != "P1" && hit.collider.tag != "P2")
                        {
                            print("I Entered Statement " + 5);
                            p2Selected[p2i - 1].GetComponent<MeshRenderer>().material.color = customGreen;
                            p2Selected[p2i - 1].tag = "Untagged";
                            p2Selected.Remove(p2Selected[p2i - 1]);
                            print(p2Selected.Count);
                            p2i--;
                            print("p2i is " + p2i);
                            p2Selected.Add(hit.collider.gameObject);
                            p2Selected[p2i].GetComponent<MeshRenderer>().material.color = Color.red;
                            p2Selected[p2i].tag = "P2";
                            print(p2Selected.Count);
                            p2i++;
                            print("p2i is " + p2i);
                            print("I Exited Statement " + 5);
                            return;
                        }
                        else if (p2 && !p2TurnThreePlus && !p2TurnError && hit.collider.tag != "P1" && hit.collider.tag != "P2" && p2i < 3)
                        {
                            print("I Entered Statement " + 6);
                            p2Selected.Add(hit.collider.gameObject);
                            p2Selected[p2i].GetComponent<MeshRenderer>().material.color = Color.red;
                            p2Selected[p2i].tag = "P2";
                            print(p2Selected.Count);
                            if (p2i < 3)
                            {
                                p2i++;
                            }
                            print("p2i is " + p2i);
                            p2TurnError = true;
                            turnOver = true;

                            if (p2i == 3)
                            {
                                p2TurnThreePlus = true;
                            }
                            print("I Exited Statement " + 6);
                            return;
                        }
                    }
                }
            }
        }
    }
}
